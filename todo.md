# Retour sur TaleTree

- Grisé avec un sens interdit les boutons bloqués
- Changer la redirection après écriture
- Mettre sur pause actif partout
- Baisser le son des audios
- Bug d'upload sur les fond d'écran et les fonds 
	- Ajout barre de progression ?
- Bug retour sur la première page
- Augmenter la taille de l'écran
- Histoire trop lourde ?
	- Séparer le premier texte en deux fragments pour ne pas décourager le lecteur
- Voix facultative pour l'audio
- Varier les fonds et les audios sur les premières pages
- Orthographe
  - « toute la maisonnée est réveillée » avec « ée »
  - AgruméE
- Monter l'épisode précédent dans l'éditeur
- Lien en gras pour les passages ayant été édité par l'utilisateur OU lien en gras pour les derniers passages édités
- Préparer un trailer