<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Id\AssignedGenerator;
use App\Entity\Chapter;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
		foreach($this->getData() as $chapter)
		{
			$manager->persist($chapter);
		}

        $metadata = $manager->getClassMetaData(get_class($chapter));
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new AssignedGenerator());

        $manager->flush();
        $manager->clear();
	}
	
	public function getData(): iterable
	{
		$path = 'src/DataFixtures/Data.json';
		$array = json_decode(file_get_contents($path), TRUE);

		$currentDateTime = new \DateTime();

		foreach($array as $chap)
		{
			$chapter = new Chapter();

			$chapter->setUpdatedAt($currentDateTime);
			$chapter->setCreatedAt($currentDateTime);

            $chapter->setId($chap['id']);
			$chapter->setTitle($chap['title']);
			$chapter->setContent($chap['content']);
			$chapter->setBackground($chap['background']);
			$chapter->setAudio($chap['audio']);
			$chapter->setPreviousChapter($chap['previousChapter']);
            $chapter->setAddingSuiteAuthorized($chap['addingSuiteAuthorized']);
			$chapter->setEditingAuthorized($chap['editingAuthorized']);
			$chapter->setTextSpeech($chap['text_speech']);

			yield $chapter;
		}
	}
}
