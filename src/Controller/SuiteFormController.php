<?php

namespace App\Controller;

use App\Entity\Chapter;
use App\Entity\ChapterBackup;
use App\Entity\Medias;
use App\Form\ChapterType;
use App\Repository\ChapterBackupRepository;
use App\Repository\ChapterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use duncan3dc\Speaker\TextToSpeech;
use duncan3dc\Speaker\Providers\VoiceRssProvider;



class SuiteFormController extends AbstractController
{

    /**
     * @var ChapterRepository
     */
    private $repository;
    /**
     * @var ChapterBackupRepository
     */
    private $chapterBackupRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ChapterRepository $repository, ChapterBackupRepository $chapterBackupRepository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->chapterBackupRepository = $chapterBackupRepository;
        $this->em = $em;
    }

    public function index(Request $request): Response
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('go-to-suite-form', $submittedToken)) {
            return $this->render('pages/suiteForm.html.twig', [
                'previousChapter' => $this->repository->findById($request->get('tale-id'))
            ]);
        }

        return $this->redirect("/tale");
    }

    public function addNewSuite(Request $request): Response {

        $DBUpdatedSuccessfully = $this->formSubmitted($request, new Chapter());

        if ($DBUpdatedSuccessfully) {
            return $this->redirect("tale/" . $this->repository->findLastChapter()->getId());
        }

        return $this->render('pages/suiteForm.html.twig', [
            'previousChapter' => $this->repository->findById($request->get('chapter')['previousChapter']),
            'DBUpdatedSuccessfully' => $DBUpdatedSuccessfully
        ]);
    }

    public function editTale(Chapter $chapter, Request $request): Response {
        return $this->render('pages/suiteForm.html.twig', [
            'chapter' => $chapter,
            'previousChapter' => $this->repository->findById($chapter->getPreviousChapter())
        ]);
    }

    public function editTaleSubmitted(Request $request): Response {

        $chapter = $this->repository->findById($request->get('chapter')['id']);
        $DBUpdatedSuccessfully = $this->formSubmitted($request, $chapter, true);

        if ($DBUpdatedSuccessfully) {
            return $this->redirect("tale/" . $chapter->getId());
        }

        return $this->render('pages/suiteForm.html.twig', [
            'chapter' => $chapter,
            'previousChapter' => $this->repository->findById($chapter->getPreviousChapter()),
            'DBUpdatedSuccessfully' => $DBUpdatedSuccessfully
        ]);
    }

    private function formSubmitted(Request $request, Chapter $currentChapter, bool $isUpdate=false): bool {

        $dbUpdateSuccess = false;
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('suite-form', $submittedToken)) {

            if ($isUpdate)  {
                $chapterBackup = TaleBackupController::getChapterBackup($currentChapter);
                $this->em->persist($chapterBackup);
            }

            $currentDateTime = new \DateTime();

            $currentChapter->setTitle($request->get('chapter')['title']);
            $currentChapter->setContent($request->get('chapter')['content']);
            $currentChapter->setUpdatedAt($currentDateTime);

            if (!$isUpdate)  {
                $currentChapter->setCreatedAt($currentDateTime);
                $currentChapter->setPreviousChapter($request->get('chapter')['previousChapter']);
                $currentChapter->setEditingAuthorized(true);
                $currentChapter->setAddingSuiteAuthorized(true);
            }

            // Voice speech controller
            if ($currentChapter->getContent() != "") {
                $provider = new VoiceRssProvider("e3a691d8b5c44fc6af58a2b893842d46", "fr-fr");
                $str = $request->get('chapter')['content'];
                $str = str_replace("</p>", "", $str);
                $pos = strpos($str, "<p");

                while ($pos !== false) {
                    $del = "";

                    for ($i = $pos; $i < strlen($str); $i++){
                        $del .= $str[$i];
                        if ($str[$i] == ">") {
                            break;
                        }
                    }
                    
                    $str = str_replace($del, "", $str);
                    $pos = strpos($str, "<p");
                }
                $tts = new TextToSpeech($str, $provider);
                $filename = "";
                if ($currentChapter->getTextSpeech() == "") {
                    $filename = "media/voices/tale_".uniqid().".mp3";
                }
                else {
                    $filename = $currentChapter->getTextSpeech();
                }     
                $tts->save($filename);
                $currentChapter->setTextSpeech($filename);
            }
            else {
                $currentChapter->setTextSpeech("");
            }

            // Media controller
            $musicFile = $request->files->get('chapter')['audio'];
            $musicDir = $this->getParameter('music_dir');
            if ($musicFile) {
                $originalMusicFilename = pathinfo($musicFile->getClientOriginalName(), PATHINFO_FILENAME);
                $originalMusicFilename = str_replace(' ', '-', $originalMusicFilename);
                $newMusicFilename = $originalMusicFilename.'-'.uniqid().'.'.$musicFile->guessExtension();
                $musicFile->move(
                    $musicDir,
                    $newMusicFilename
                );
                $currentChapter->setAudio('media/music/'.$newMusicFilename);
            }
            else {
                if (!$currentChapter->getAudio()) {
                    $currentChapter->setAudio('media/music/default.mp3');
                }
            }

            $imageFile = $request->files->get('chapter')['background'];
            $imageDir = $this->getParameter('images_dir');
            if ($imageFile) {
                $originalImageFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $originalImageFilename = str_replace(' ', '-', $originalImageFilename);
                $newImageFilename = $originalImageFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $imageDir,
                    $newImageFilename
                );
                $currentChapter->setBackground('media/images/'.$newImageFilename);
            }
            else {
                if (!$currentChapter->getBackground()) {
                    $currentChapter->setBackground('media/images/default.jpg');
                }
            }

            $this->em->persist($currentChapter);
            $this->em->flush();

            $dbUpdateSuccess = true;
        }

        return $dbUpdateSuccess;
    }
}