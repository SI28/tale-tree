<?php

namespace App\Controller;

use App\DataFixtures\AppFixtures;
use App\Repository\ChapterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
     * @var ChapterRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ChapterRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
	}
	
    public function index(): Response
    {
        //dump($this->repository->lastUpdate()['updatedAt']);

		return $this->render(
			'pages/home.html.twig', [
			    'firstChapter' => $this->repository->firstChapter(),
                'nbChapter' => $this->repository->nbChapter()[1],
                'nbImages' => $this->repository->nbImages()[1],
				'lastUpdate' => $this->repository->lastUpdate()['updatedAt']->format('d-m-Y H:i:s'),
				'recentChapters' => $this->repository->recentChapters(10)
            ]
		);
    }
}