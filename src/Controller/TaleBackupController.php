<?php

namespace App\Controller;

use App\DataFixtures\AppFixtures;
use App\Entity\Chapter;
use App\Entity\ChapterBackup;
use App\Repository\ChapterBackupRepository;
use App\Repository\ChapterRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaleBackupController extends AbstractController
{
    /**
     * @var ChapterRepository
     */
    private $repository;
    /**
     * @var ChapterBackupRepository
     */
    private $chapterBackupRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ChapterRepository $repository, ChapterBackupRepository $chapterBackupRepository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->chapterBackupRepository = $chapterBackupRepository;
        $this->em = $em;
    }

    public function index(int $id): Response
    {
        return $this->render('pages/taleBackup.html.twig', [
            'currentChapterId' => $id,
            'lstBackup' => $this->chapterBackupRepository->findBackupById($id)
        ]);
    }

    public function getTalePreview(int $id, int $isBackup): Response
    {
        if ($isBackup) {
            $chapterBackup = $this->chapterBackupRepository->findById($id);

            return $this->render('pages/tale.html.twig', [
                'currentChapter' => $chapterBackup,
                'nextChapters' => $this->repository->findNextChapters($chapterBackup->getChapterId()),
                'preview' => true
            ]);
        }

        $chapter = $this->repository->findById($id);

        return $this->render('pages/tale.html.twig', [
            'currentChapter' => $chapter,
            'nextChapters' => $this->repository->findNextChapters($chapter->getId()),
            'preview' => true
        ]);

    }

    public function restoreBackupVersion(Request $request):Response {

        $chapterId = -1;

        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('restoreBackupForm', $submittedToken)) {
            $chapterBackup = $this->chapterBackupRepository->findById($request->request->get('backupId'));
            $chapter = $this->repository->findById($chapterBackup->getChapterId());

            $this->restoreChapter($chapter, $chapterBackup);

            $chapterId = $chapter->getId();

        }

        return ($this->redirect('tale/' . $chapterId));
    }

    public static function getChapterBackup(Chapter $currentChapter) {
        $chapterBackup = new ChapterBackup();

        $chapterBackup->setChapterId($currentChapter->getId())
            ->setTitle($currentChapter->getTitle())
            ->setContent($currentChapter->getContent())
            ->setCreatedAt($currentChapter->getCreatedAt())
            ->setUpdatedAt($currentChapter->getUpdatedAt())
            ->setPreviousChapter($currentChapter->getPreviousChapter())
            ->setBackground($currentChapter->getBackground())
            ->setAudio($currentChapter->getAudio())
            ->setAddingSuiteAuthorized($currentChapter->getAddingSuiteAuthorized())
            ->setEditingAuthorized($currentChapter->getEditingAuthorized())
            ->setTextSpeech($currentChapter->getTextSpeech());

        return $chapterBackup;
    }

    private function restoreChapter(Chapter $chapter, ChapterBackup $chapterBackup) {
        $newChapterBackup = TaleBackupController::getChapterBackup($chapter);

        $chapter->setTitle($chapterBackup->getTitle());
        $chapter->setContent($chapterBackup->getContent());
        $chapter->setCreatedAt($chapterBackup->getCreatedAt());
        $chapter->setUpdatedAt(new DateTime());
        $chapter->setPreviousChapter($chapterBackup->getPreviousChapter());
        $chapter->setBackground($chapterBackup->getBackground());
        $chapter->setAudio($chapterBackup->getAudio());
        $chapter->setTextSpeech($chapterBackup->getTextSpeech());
        $chapter->setAddingSuiteAuthorized($chapterBackup->getAddingSuiteAuthorized());
        $chapter->setEditingAuthorized($chapterBackup->getEditingAuthorized());

        $this->em->persist($newChapterBackup);
        $this->em->persist($chapter);
        $this->em->flush();
    }
}