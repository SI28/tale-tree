<?php

namespace App\Controller;

use App\Entity\Chapter;
use App\Repository\ChapterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaleController extends AbstractController
{

    /**
     * @var ChapterRepository
     */
    private $repository;

    public function __construct(ChapterRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Chapter $chapter, Request $request): Response
    {
        return $this->render('pages/tale.html.twig', [
            'currentChapter' => $chapter,
            'nextChapters' => $this->repository->findNextChapters($chapter->getId()),
            'preview' => false
        ]);
    }
}