<?php

namespace App\Controller;

use App\DataFixtures\AppFixtures;
use App\Repository\ChapterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class CreditsController extends AbstractController
{

    public function index(): Response
    {
        return $this->render('pages/credits.html.twig');
    }
}
