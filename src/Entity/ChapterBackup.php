<?php

namespace App\Entity;

use App\Repository\ChapterBackupRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChapterBackupRepository::class)
 */
class ChapterBackup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $chapterId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $previousChapter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $background;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $audio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $addingSuiteAuthorized;

    /**
     * @ORM\Column(type="boolean")
     */
    private $editingAuthorized;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textSpeech;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChapterId(): ?int
    {
        return $this->chapterId;
    }

    public function setChapterId(int $chapterId): self
    {
        $this->chapterId = $chapterId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPreviousChapter(): ?int
    {
        return $this->previousChapter;
    }

    public function setPreviousChapter(int $previousChapter): self
    {
        $this->previousChapter = $previousChapter;

        return $this;
    }

    public function getBackground(): ?string
    {
        return $this->background;
    }

    public function setBackground(string $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getAudio(): ?string
    {
        return $this->audio;
    }

    public function setAudio(string $audio): self
    {
        $this->audio = $audio;

        return $this;
    }

    public function getAddingSuiteAuthorized(): ?bool
    {
        return $this->addingSuiteAuthorized;
    }

    public function setAddingSuiteAuthorized(bool $addingSuiteAuthorized): self
    {
        $this->addingSuiteAuthorized = $addingSuiteAuthorized;

        return $this;
    }

    public function getEditingAuthorized(): ?bool
    {
        return $this->editingAuthorized;
    }

    public function setEditingAuthorized(bool $editingAuthorized): self
    {
        $this->editingAuthorized = $editingAuthorized;

        return $this;
    }

    public function getTextSpeech(): ?string
    {
        return $this->textSpeech;
    }

    public function setTextSpeech(?string $textSpeech): self
    {
        $this->textSpeech = $textSpeech;

        return $this;
    }
}
