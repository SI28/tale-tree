<?php

namespace App\Repository;

use App\Entity\Chapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Chapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chapter[]    findAll()
 * @method Chapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChapterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chapter::class);
    }

    public function findById($id)
    {
        $result = $this->createQueryBuilder('c')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()[0]
        ;
//        if ($result != null) {
//            return $result;
//        }
//        return array();
        return $result;
    }

    public function firstChapter()
    {
		$result = $this->createQueryBuilder('c')
			->select('c.id')
			->andWhere('c.previousChapter = -1')
			->orderBy('c.id')
			->setMaxResults(1)
            ->getQuery()
            ->getResult()
		;
       if ($result == null) {
           return -1;
       }
        return $result[0]["id"];
    }

    public function findNextChapters($currentChapterId)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.previousChapter = :currentChapterId')
            ->setParameter('currentChapterId', $currentChapterId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLastChapter() {
        $result = $this->createQueryBuilder('c')
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;

        return $result[0];
    }

    public function recentChapters(int $limit) {
        $result = $this->createQueryBuilder('c')
            ->orderBy('c.updatedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;

        return $result;
    }

    public function nbChapter() {
        $result = $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->getQuery()
            ->getResult()
        ;

        return $result[0];
    }

    public function nbImages() {
        $result = $this->createQueryBuilder('c')
            ->select('COUNT(DISTINCT c.background)')
            ->getQuery()
            ->getResult()
        ;

        return $result[0];
    }

    public function lastUpdate() {
        $result = $this->createQueryBuilder('c')
            ->select('c.updatedAt')
            ->orderBy('c.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;

        return $result[0];
    }


    // /**
    //  * @return Chapter[] Returns an array of Chapter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chapter
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
