<?php

namespace App\Repository;

use App\Entity\ChapterBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChapterBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChapterBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChapterBackup[]    findAll()
 * @method ChapterBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChapterBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChapterBackup::class);
    }

    public function findById($id)
    {
        $result = $this->createQueryBuilder('c')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
        if ($result != null) {
            return $result[0];
        }
        return null;
    }

    public function findBackupById($id)
    {
        $result = $this->createQueryBuilder('c')
            ->andWhere('c.chapterId = :id')
            ->setParameter('id', $id)
            ->orderBy('c.updatedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
        if ($result != null) {
            return $result;
        }
        return null;
    }

    // /**
    //  * @return ChapterBackup[] Returns an array of ChapterBackup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChapterBackup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
