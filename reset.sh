#!/bin/sh

# Delete database
bin/console d:d:d --force
# Create database
bin/console d:d:c
# Create relations
bin/console d:s:c
# Insert data
bin/console d:f:l --no-interaction