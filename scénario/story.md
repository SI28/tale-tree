# La planète agrumé
Sur un citron cosmique orbitant autour d’une mangue interstellaire, se trouve une cité-état nauséabonde nommée Durian. Ses habitants, les duriens, sont des êtres crasseux à l’allure de vieille poire ratatinée. Outre la culture d’asticot, leur principale occupation consiste à prier pour le retour du saint poirier. Le reste du temps, les duriens guerroient contre les barbares, des figues guerrières qui vivent à quelques lieues de là.

Au cœur de la ville domine un grand château aux douves profondes d’où émane l’odeur pestilentielle de la ville. Les grandes décisions politiques y sont prises par le roi William III. Parmi les membres les plus éminents de la cour, la stratège Berthe aux grands pieds, très pieuse, est très reconnue pour ses nombreuses victoires éclatantes.

Au cœur de la ville domine un grand château aux douves profondes d’où émane l’odeur pestilentielle de la ville, les corps des prisonniers de guerre y pourrissant. Les grandes décisions politiques y sont prises par le roi William III. Parmi les membres les plus éminents de la cour, la stratège Berthe aux grands pieds, très pieuse, est très reconnue pour ses nombreuses victoires éclatantes.

À la frontière est de la cité, le grand royaume de Croncel, principalement peuplé de pommes s’étend jusqu’à une vaste forêt.

Maintenant, c’est à vous de jouer. Qui être vous ?
	
- Une pomme à la cour de Durian
- Une poire paysanne

---

## Une pomme à la cour de Durian

Depuis votre plus tendre enfance, vous avez vécu avec votre frère et vos parents dans le royaume de Croncels dirigé par la bien aimée reine Blanche Calville. Vos parents, monsieur et madame D’Apis, sont de riches drapiers ayant construit leur renommée sur la vente de tapis gris. Pour accéder à des titres de noblesse, vous avez été marié à un vieux Duc tuberculeux qui poussa son dernier souffle quelques semaines seulement après le mariage. Avant de rejoindre le Grand Terreau1, il vous a introduit à la cour. Vous avez eu la chance de connaître la douceur de vivre de la cour de Croncel pendant deux belles années.

Durant ceux deux années, les relations diplomatiques avec Durian, la cité des poires, se sont quelque peu dégradées. En effet, une délégation armée de Durian s’est à plusieurs reprises aventurée sur les terres de Croncel afin de collecter illégalement des taxes sur les paysans. Afin d’éviter une guerre, il a été décidé que chaque partie enverrait une ambassade le temps qu’un accord soit trouvé.

C’est ainsi qu’un matin d’hiver, Sa Majesté Calville, vous désigna pour faire partie de l’ambassade qui doit partir dès le lendemain.

- Vous refusez de partir
- Vous préparez vos affaires

---
## Vous refusez de partir

Sa Majesté Calville vous explique à quel point ce départ présente une opportunité pour vous, mais vous déclinez tout de même son offre. En effet, malgré votre envie de découvrir la mystérieuse cité-état de Durian et ses légendes, vous êtes bien conscient qu’un traquenard vous attend probablement là-bas. À ce moment, elle désigne alors votre frère à votre place qui accepte sans la moindre hésitation.

Étant très proche de votre frère, il vous vient alors une idée. Lors d’un adieu touchant à votre frangin, vous lui faite promettre de vous écrire souvent. De cette façon, vous pourrez suivre les actualités de Durian à distance, et peut-être percer certains de ses mystères.

---
## Vous préparez vos affaires

De joie, vous remerciez Sa Majesté Calville. Vous courez à vos appartements afin de préparer vos bagages. Vous remplissez une première malle uniquement avec vos vêtements, pour faire face à toutes les occasions. Dans une seconde valise, vous emportez de quoi vous soigner ayant entendu de nombreuses rumeurs concernant le retard considérable en médecine des duriens. Prudent, vous y glissez également un petit couteau.

Hélas, au petit matin, vous apprenez que vous n’étiez censé prendre qu’un seul bagage pour des raisons de place. Le départ étant imminent, vous devez choisir ce que vous emporterez.

- Prendre les vêtements
- Prendre la valise de premier secours

---

## Une poire paysanne

Vous êtes une jeune poire élevée dans les valeurs traditionnelles duriennes, attachée à la ferme familiale. Vous avez grandi aux côtés de votre père qui vous a enseigné l’art de la traite des asticots.

Une légende, très encré dans la région, raconte que si l’on ne laisse pas un grand bidon de lait sur le rebord de la fenêtre les soirs de nouvelle Lune, alors Zottegem, une grande poule à la tête noire, viendrait dévorer tous les asticots. Chaque lendemain de nouvelle Lune, on retrouve le bidon vide après le passage de la terrible Zottegem.

Agacé par cette poule de malheur, vous décidez de mettre des somnifères dans le lait. Or cette nuit-là toute la maisonnée est réveillé par un vacarme terrible semblant provenir de l’asticotier.

- Vous décidez d’aller jeter un œil
- Vous décidez de sonner l’alarme pour rameuter le village

---

## Vous décidez d’aller jeter un œil

Sans faire de bruit, vous vous laissez guider par les mugissements de panique des asticots. En vous approchant, vous apercevez des faisceaux lumineux par intermittence provenant de la grange à outil. Très discrètement, vous entrez dans la grange à outil et entrevoyez deux poires. En regardant mieux, vous vous rendez compte qu’il y a du jus partout, l’une des deux poires s’étant visiblement blessée.

- Proposer votre aide
- Attraper une arme et les interpeler

---
## Vous décidez de sonner l’alarme pour rameuter le village
Vous montez sur le toit de la ferme et hurlez à plein poumons. À ce moment, les lumières s’allument dans les fermes alentours. Depuis le toit, vous scrutez les environs pour tenter de repérer tout moment suspect. Durant un bref instant, il vous semble avoir vu du coin de l’œil une ombre silencieuse courir vers la grange.

Le temps de redescendre, d’expliquer la situation aux voisins, et de vous rendre à la grange, il n’y a plus rien à voir. Pourtant, l’un des voisins repère du jus frais sur le sol. Visiblement, quelqu’un était ici et s’est sérieusement blessé.
	
- Inspecter le reste de la grange
- Organiser une battue avec des volontaires