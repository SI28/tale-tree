# PROJET SI28 A20 : Tail Tree : un récit ouvert aux contributions des internautes
Louis PINEAU, Nu Huyen Trang PHAM et Florent GAUDIN

# Installation

    $ git clone https://gitlab.utc.fr/SI28/tale-tree.git
    $ cd tale-tree
    $ composer install
    
# Lancer le serveur

    $ php -S 127.0.0.1:8000 -t public
    
# Reconstuire la base de données

    $ php bin/console doctrine:schema:update --force
    
# Chargé le jeu de données initial dans la base de données

    $ php bin/console doctrine:fixtures:load --append
    
# Structure du projet Symfony

**config/routes.yaml**
> Contient les différentes routes de notre site (association URL / controller)

**public/**
> Contient les ressources visibles côté client (css, javascript, images, etc)

**templates/**
> Contient les templates des différentes pages html.
>
> Chaque page doit aux moins hériter de base.html.twig (sauf exception)

**src/**
> Contient le code côté serveur (du modèle et du controller)

**.env**

    APP_ENV=dev
> Le site sera affiché en mode développement (avec la debug bar de Symfony entre autres)

    APP_ENV=prod
> Le site sera affiché en mode production (sans la debug bar de Symfony par exemple)
    
    DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
> On indique ici les informations de connexion à la base de données

# Note d'intention
## Concept
La platforme permet d'écrire des histoires à **choix multiple** de façon **contributive**. Chaque lecteur a en effet la possibilité d'ajouter un choix ou un passage ou bien d'en modifier un existant. Une histoire de base sera présente, le joueur pourra alors suivre celle-ci et la **modifier** s'il le souhaite (modifier légérement l'histoire existante ou créer une nouvelle branche de toute pièce) mais il pourra aussi créer une nouvelle histoire qui sera mis en ligne sur le site et dont les autres joueurs pourront venir la lire et la modifier.
### Modération
Si tout le monde a la possibilité de modifier, toutes les modifications ne se valent pas. Un utilisateur a la possibilité de se créer un compte (**non obligatoire pour éditer**) qui lui permettra de lier ses contributions. Chaques modifications pertinantes lui rapportera des **points de confiance**. Une fois un certain score atteint, il aura la possibilité de modérer les modifications/ajouts des autres utilisateurs.
Toutes les modifications sont archivées. De cette façon, en cas de modification indésirable, le texte original n'est pas perdu. 
### Onglet Discussion
Les utilisateurs peuvent regarder un récit d'un autre et faire des commentaires. Tous les commentaires feront une discussion qui contribuera à l'amélioration du contenu de l'histoire. Même principe de Modération, chaques commentaires obtenant une émotion d'un autre (:heart:, :thumbsup:, :smiley:, :clap:) rapportera des **points de confiance**. Lorsque un certain score atteint, l'utilisateur peut obtenir un badge. 
### Combats
Certaines histoires peuvent inclure des combats. À cet effet, le joueur dispose d'une jauge de vie qui est affiché à compter du moment où il perd de la vie pour la première fois. Cette jauge peut aussi avoir une influence sur les choix affichés. L'issue du combat aura un **impact sur la suite de l'histoire**.

### Inventaire
Certains choix permetront au lecteur d'aquérir des objets qui pourront **influencer les choix affichés** dans le récit. L'inventaire est affiché en permanance à côté du texte. Les objets sont affichés sous la forme d'une image qui aura été postée par l'écrivain.

### Personnalisation de l'histoire
Le joueur peut changer le pays, l'époque ce qui aura pour conséquence de **changer plusieurs petits morceaux de l'histoire**, sans impact. Les utilisateurs pourront par exemple associer le pays "France" à plusieurs morceaux d'histoire. Si le joueur décide qu'il se trouve en France alors ces morceaux seront intégrés à l'histoire en remplacement des anciens morceaux équivalents.
Par ailleurs, le joueur peut changer les personnages (nom, caractéristiques, etc.) qui sera mis à jours automatiquement dans les scènes suivantes. 

### Ambiance musicale
L'utilisateur peut ajouter une musique qui va **accompagner la lecture** d'un morceau de l'histoire afin de **créer une meilleure immersion**. S'il le souhaite il pourra également proposer une version audio de cette histoire. Le côté contributif du site permettra de récupérer l'audio pour l'éditer en ajoutant sa voix afin de contribuer à une meilleure immersion (voix de femme si le morceau de l'histoire est raconté par une femme, voix grave pour un méchant par exemple, etc).

### Images
Les récits peuvent intégrer des images, que ce soit des images de fond ou bien placées au sein du récit.

## Public-cible
Personnes ayant un intérêt pour l'écriture:
1. Sans connaissance préalable sur le jeu de rôle. Cette catégorie de personnes aura besoins d'un tutoriel introductif sur le jeu de rôle et sur le fonctionnement du site.
2. Connaissant le jeu de rôle mais sans expérience avec la platforme. L'introduction ne contiendra que des éléments techniques sur l'utilisation de la platforme.
3. Personnes étant interéssées par la lecture et le jeu de rôle. Pas d'introduction particulière.

Mode de consultation : sur PC ou Smartphone
Mode d'édition: sur PC
## Objectifs
- Chaque lecteur aura la possibilité de vivre une expérience imersive et personnalisée.
- Ce jeu permet d'améliorer, de développer l'imagination.
- Il permet aussi de construire à plusieurs, de développer l'entraide.
- Certaines histoires permettront de découvrir de nouvelles cultures.

# TODO

## HTML/CSS
- [ ] [Florent] Tale_9 l'affiche n'est pas top il faudrait mettre des tailles min

## JS 
- [ ] [Louis] Lien en gras pour les passages ayant été édité par l'utilisateur OU ~~lien en gras pour les derniers passages édités~~

## PHP
- [ ] [LOUIS] Système « les nouveaux passages ajouté » sur la page d’accueil pour savoir rapidement ce qui a été fait récemment (Utilisation modale)
- [ ] [FLORENT] Error quand on appuie sur bouton back au niveau de tale_1

## HTML / CSS / PHP
- [x] [TRANG] Ajout d’un background et d’une musique qui ne fonctionne pas toujours
    - la pop-up pour ajouter ces éléments ne s’ouvre pas toujours
    - l'upload ne fonctionne pas ou n'a pas le temps de se terminer quand l'utilisateur revient sur la page précédente
    --> L'ajout d'un spinner lors de téléverser les médias

## JSON
- [ ] [LOUIS] Ajouter les passages des camarades de SI28

## Autres
- [x] [TRANG] Voix particulières et bruitage OK mais pas de lecture en entier des passages
