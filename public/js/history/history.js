const HISTORY_STORAGE = "history"

function saveStep() {
	let hc = localStorage.getItem(HISTORY_STORAGE);
	if(hc == null) {
		hc = {};
	} else {
		hc = JSON.parse(hc);
	}

	let title = $('#left-part article h2').html();
	let id = $('#chapid').html();
	let previd = $('#previd').html();

	let current = {t: title, p: previd};
	hc[id] = current;

	let content = "";
	while(!!current) {
		content += `<li><a href="${id}">${current.t}</a></li>`;
		id = current.p;
		current = hc[id];
	}
	content = `<ul>${content}</ul>`;
	let el = $('#history-panel');
	el.html(content);

	localStorage.setItem(HISTORY_STORAGE, JSON.stringify(hc));
}
function minfont() {
	const minfont = 16
	$("article *:not(#info-chapter)").each( function () {
		var $this = $(this);
		if (parseInt($this.css("fontSize")) < minfont) {
			$this.css({ "font-size": minfont+"px" });   
		}
	});
}
$( document ).ready(function() {
	minfont()
	saveStep()
});