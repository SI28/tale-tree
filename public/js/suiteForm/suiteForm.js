$(document).ready(function () {
    bsCustomFileInput.init()
})

function resetForm() {
    if ($('input#isEditingForm').val() === 'true') {
        location.reload();
    } else {
        $('input#chapter_title').val('');
        $('input#background-img-file').val('');
        $('label#background-img-file-label').text('Image de fond ...');
        $('input#audio-file').val('');
        $('label#audio-file-label').text('Ambiance sonore ...');
        $('#chapter_content').summernote('reset');
    }
}