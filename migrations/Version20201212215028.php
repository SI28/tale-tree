<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201212215028 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chapter ADD COLUMN text_speech VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__chapter AS SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio, adding_suite_authorized, editing_authorized FROM chapter');
        $this->addSql('DROP TABLE chapter');
        $this->addSql('CREATE TABLE chapter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, previous_chapter INTEGER NOT NULL, background VARCHAR(255) NOT NULL, audio VARCHAR(255) NOT NULL, adding_suite_authorized BOOLEAN NOT NULL, editing_authorized BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO chapter (id, title, content, created_at, updated_at, previous_chapter, background, audio, adding_suite_authorized, editing_authorized) SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio, adding_suite_authorized, editing_authorized FROM __temp__chapter');
        $this->addSql('DROP TABLE __temp__chapter');
    }
}
