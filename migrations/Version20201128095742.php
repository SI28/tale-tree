<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201128095742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE medias');
        $this->addSql('CREATE TABLE medias (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, media_filename VARCHAR(255) NOT NULL, src VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__chapter AS SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio FROM chapter');
        $this->addSql('DROP TABLE chapter');
        $this->addSql('CREATE TABLE chapter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, content CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, previous_chapter INTEGER NOT NULL, background VARCHAR(255) NOT NULL, audio VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO chapter (id, title, content, created_at, updated_at, previous_chapter, background, audio) SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio FROM __temp__chapter');
        $this->addSql('DROP TABLE __temp__chapter');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE medias');
        $this->addSql('CREATE TEMPORARY TABLE __temp__chapter AS SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio FROM chapter');
        $this->addSql('DROP TABLE chapter');
        $this->addSql('CREATE TABLE chapter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, previous_chapter INTEGER NOT NULL, background INTEGER DEFAULT NULL, audio INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO chapter (id, title, content, created_at, updated_at, previous_chapter, background, audio) SELECT id, title, content, created_at, updated_at, previous_chapter, background, audio FROM __temp__chapter');
        $this->addSql('DROP TABLE __temp__chapter');
    }
}
