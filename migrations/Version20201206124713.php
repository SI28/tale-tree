<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201206124713 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chapter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, previous_chapter INTEGER NOT NULL, background VARCHAR(255) NOT NULL, audio VARCHAR(255) NOT NULL)');
        $this->addSql('ALTER TABLE chapter ADD COLUMN adding_suite_authorized BOOLEAN NOT NULL DEFAULT TRUE');
        $this->addSql('ALTER TABLE chapter ADD COLUMN editing_authorized BOOLEAN NOT NULL DEFAULT TRUE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
